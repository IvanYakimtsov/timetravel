package com.yakimtsov.timetravel;


public class TimeTravelException extends RuntimeException {
    public TimeTravelException(String message) {
        super(message);
    }
}
