package com.yakimtsov.timetravel;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TIME_TRAVEL_INFO")
public class TimeTravelInfo {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "personal_galactic_id")
    private String pgi;

    @Column(name = "place")
    private String place;

    @Column(name = "travel_time")
    @Convert(converter = TimeTravelDateConverter.class)
    private LocalDateTime time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPgi() {
        return pgi;
    }

    public void setPgi(String pgi) {
        this.pgi = pgi;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}