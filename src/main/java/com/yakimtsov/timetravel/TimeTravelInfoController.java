package com.yakimtsov.timetravel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequestMapping(value = "/timetravel")
public class TimeTravelInfoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeTravelInfoController.class);

    @Autowired
    private TimeTravelInfoService timeTravelInfoService;

    @GetMapping("/timeTravels")
    public List<TimeTravelInfo> findTimeTravels() {
        return timeTravelInfoService.getTimeTravels();
    }

    @GetMapping("/{id}/timeTravels")
    public TimeTravelInfo findTimeTravelById(@PathVariable Integer id) {
        return timeTravelInfoService.getTimeTravel(id);
    }

    @PostMapping("/timeTravels")
    public int createTimeTravel(@RequestBody TimeTravelInfo timeTravelInfo) {
        return timeTravelInfoService.createTimeTravel(timeTravelInfo);
    }

    @DeleteMapping("/{id}/timeTravels")
    public void deleteTimeTravelById(@PathVariable int id) {
        timeTravelInfoService.deleteTimeTravel(id);
    }


    @ExceptionHandler(TimeTravelNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorMessage handleTimeTravelNotFoundException(TimeTravelNotFoundException ex) {

        LOGGER.error("Time travel not found", ex);
        return new ErrorMessage(BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(TimeTravelException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorMessage handleTimeTravelException(TimeTravelException ex) {

        LOGGER.error("Unable to perform time travel", ex);
        return new ErrorMessage(BAD_REQUEST.value(), ex.getMessage());
    }

    private static final class ErrorMessage {

        private final int code;
        private final String message;

        private ErrorMessage(int code, String message) {

            this.code = code;
            this.message = message;
        }

        public int getCode() {

            return code;
        }

        public String getMessage() {

            return message;
        }
    }
}
