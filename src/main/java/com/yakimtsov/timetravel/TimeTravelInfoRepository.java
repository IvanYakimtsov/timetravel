package com.yakimtsov.timetravel;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface TimeTravelInfoRepository extends JpaRepository<TimeTravelInfo, Integer> {

    List<TimeTravelInfo> findByPgiAndTime(String pgi, LocalDateTime dateTime);
}
