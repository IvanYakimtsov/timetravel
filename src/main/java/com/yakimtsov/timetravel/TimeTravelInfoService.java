package com.yakimtsov.timetravel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TimeTravelInfoService {

    @Autowired
    private TimeTravelInfoRepository repository;

    @Autowired
    private TimeTravelInfoValidator validator;

    public List<TimeTravelInfo> getTimeTravels() {
        return repository.findAll();
    }

    public TimeTravelInfo getTimeTravel(int id) {
        return repository.findById(id)
                .orElseThrow(() -> new TimeTravelNotFoundException("Time travel with specified id not found"));
    }

    public int createTimeTravel(TimeTravelInfo timeTravelInfo) {
        validator.validate(timeTravelInfo);
        return repository.save(timeTravelInfo).getId();
    }

    public void deleteTimeTravel(int id) {
        repository.deleteById(id);
    }
}
