package com.yakimtsov.timetravel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class TimeTravelInfoValidator {

    @Autowired
    private TimeTravelInfoRepository repository;

    public void validate(TimeTravelInfo timeTravelInfo) {
        if (timeTravelInfo.getPlace() == null || timeTravelInfo.getPlace().isEmpty()) {
            throw new TimeTravelException("Place is not specified");
        }

        if (timeTravelInfo.getTime() == null) {
            throw new TimeTravelException("Time is not specified");
        }

        if (timeTravelInfo.getPgi() != null) {
            Pattern p = Pattern.compile("[A-Z0-9]{5,10}");
            Matcher m = p.matcher(timeTravelInfo.getPgi());
            if (!m.matches()) {
                throw new TimeTravelException("Personal galactic identifier is not valid");
            }
            List<TimeTravelInfo> paradox =
                    repository.findByPgiAndTime(timeTravelInfo.getPgi(), timeTravelInfo.getTime());
            if(!paradox.isEmpty()){
                throw new TimeTravelException("Paradox is not allowed");
            }
        } else {
            throw new TimeTravelException("Personal galactic identifier is not specified");
        }
    }
}
