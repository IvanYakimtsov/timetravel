package com.yakimtsov.timetravel;

public class TimeTravelNotFoundException extends RuntimeException {
    public TimeTravelNotFoundException(String message) {
        super(message);
    }
}
