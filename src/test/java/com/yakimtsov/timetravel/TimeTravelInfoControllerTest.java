package com.yakimtsov.timetravel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.time.Month;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TimeTravelInfoControllerTest {

    @Autowired
    private TimeTravelInfoController controller;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private TimeTravelInfoRepository repository;

    private TimeTravelInfo info;

    @BeforeEach
    private void init() {
        info = new TimeTravelInfo();
        info.setTime(LocalDateTime.of(1986, Month.APRIL, 8, 12, 30));
        info.setPgi("XXXX1");
        info.setPlace("Krakow");
    }


    @Test
    public void createTimeTravelsTest() throws Exception {
        mvc.perform(post("/timetravel/timeTravels")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"pgi\": \"DSA23DE\", \"place\": \"Krakow\", \"time\": \"2012-04-23T18:25:43.511Z\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void createTimeTravelsNoPlaceTest() throws Exception {
        mvc.perform(post("/timetravel/timeTravels")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"pgi\": \"DSA23DE\", \"time\": \"2012-04-23T18:25:43.511Z\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void createTimeTravelsNoTimeTest() throws Exception {
        mvc.perform(post("/timetravel/timeTravels")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"pgi\": \"DSA23DE\", \"place\": \"Krakow\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void createTimeTravelsInvalidPgiTest() throws Exception {
        mvc.perform(post("/timetravel/timeTravels")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"pgi\": \"DSA23D!E\", \"place\": \"Krakow\", \"time\": \"2012-04-23T18:25:43.511Z\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }


    @Test
    public void getAllTimeTravelsTest() throws Exception {
        repository.save(info);
        mvc.perform(MockMvcRequestBuilders
                .get("/timetravel/timeTravels")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
        repository.deleteById(info.getId());
    }

    @Test
    public void getTimeTravelsByIdTest() throws Exception {
        int id = repository.save(info).getId();
        mvc.perform(MockMvcRequestBuilders
                .get("/timetravel/" + id + "/timeTravels")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        repository.deleteById(info.getId());
    }

    @Test
    public void getTimeTravelsByIdNotFoundTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/timetravel/11111111/timeTravels")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteTimeTravelTest() throws Exception {
        int id = repository.save(info).getId();
        mvc.perform(MockMvcRequestBuilders
                .delete("/timetravel/" + id + "/timeTravels")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
