package com.yakimtsov.timetravel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class TimeTravelInfoServiceTest {
    @Spy
    private TimeTravelInfoRepository repository;

    @Spy
    @InjectMocks
    private TimeTravelInfoValidator validator;

    @InjectMocks
    private TimeTravelInfoService service;

    private TimeTravelInfo info;

    @BeforeEach
    private void init() {
        this.info = new TimeTravelInfo();
        info.setId(1);
        info.setTime(LocalDateTime.of(1986, Month.APRIL, 8, 12, 30));
        info.setPgi("XXXX1");
        info.setPlace("Krakow");
    }

    @Test
    public void getAllTimeTravelsTest() {
        Mockito.when(repository.findAll()).thenReturn(Collections.singletonList(info));
        List<TimeTravelInfo> result = service.getTimeTravels();
        assertEquals(1, result.size());
        assertEquals(1, result.get(0).getId());
    }

    @Test
    public void getTimeTravelByIdTest() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.of(info));
        TimeTravelInfo result = service.getTimeTravel(1);
        assertEquals(1, result.getId());
    }

    @Test
    public void getTimeTravelByIdNotFoundTest() {
        Mockito.when(repository.findById(any())).thenReturn(Optional.empty());
        assertThrows(TimeTravelNotFoundException.class, () -> service.getTimeTravel(1));
    }


    @Test
    public void createTimeTravelTest() {
        Mockito.when(repository.save(any())).thenReturn(info);
        int result = service.createTimeTravel(info);
        assertEquals(1, result);
    }

    @Test
    public void createTimeTravelParadoxTest() {
        Mockito.when(repository.save(any())).thenReturn(info);
        Mockito.when(repository.findByPgiAndTime(any(), any())).thenReturn(Collections.singletonList(info));
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void createTimeTravelEmptyPlaceTest() {
        info.setPlace(null);
        Mockito.when(repository.save(any())).thenReturn(info);
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void createTimeTravelEmptyTimeTest() {
        info.setTime(null);
        Mockito.when(repository.save(any())).thenReturn(info);
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void createTimeTravelEmptyPgiTest() {
        info.setPgi(null);
        Mockito.when(repository.save(any())).thenReturn(info);
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void createTimeTravelShortPgiTest() {
        info.setPgi("X");
        Mockito.when(repository.save(any())).thenReturn(info);
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void createTimeTravelLongPgiTest() {
        info.setPgi("XXXXXXXXXXXXXXXXXXXXXXXXX");
        Mockito.when(repository.save(any())).thenReturn(info);
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void createTimeTravelInvalidPgiTest() {
        info.setPgi("XXX_111");
        Mockito.when(repository.save(any())).thenReturn(info);
        assertThrows(TimeTravelException.class, () -> service.createTimeTravel(info));
    }

    @Test
    public void deleteTimeTravelTest() {
        service.deleteTimeTravel(1);
    }
}
